import Vue from 'vue';
import Vuetify from 'vuetify/lib';
import colors from 'vuetify/lib/util/colors';
import { en, ru } from 'vuetify/lib/locale';

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: 'mdi'
  },
  lang: {
    locales: { en, ru },
    current: 'ru'
  },
  theme: {
    themes: {
      light: {
        primary: colors.indigo.darken1,
        secondary: colors.pink.darken1,
        accent: colors.yellow.darken2,
        error: colors.deepOrange.darken1,
        background: colors.shades.white,
        appBar: colors.indigo.lighten1,
        navigationDraw: colors.grey.lighten3,
        information: colors.grey.darken4
      },
      dark: {
        primary: colors.amber.accent3,
        secondary: colors.pink.accent1,
        accent: colors.yellow.darken2,
        error: colors.deepOrange.accent2,
        background: colors.grey.darken4,
        appBar: colors.grey.darken3,
        navigationDraw: colors.grey.darken3,
        information: colors.grey.lighten2
      }
    }
  }
});
