import * as firebase from 'firebase';
import Profile from '../data/profile';
import { formatDateTime } from '../utils/date';

export default {
  state: {
    profile: null,
    profileList: []
  },

  mutations: {
    setProfile(state, { profile }) {
      state.profile = profile;
    },

    setProfileList(state, { profiles }) {
      state.profileList = profiles;
    }
  },

  actions: {
    async registerNew({ commit, dispatch }, { email, password }) {
      commit('setLoading', { loading: true });
      try {
        const userFb = await firebase
          .auth()
          .createUserWithEmailAndPassword(email, password);

        const profile = new Profile(
          userFb.user.uid,
          userFb.user.email,
          userFb.user.emailVerified,
          userFb.user.displayName,
          userFb.user.photoURL,
          formatDateTime(new Date(userFb.user.metadata.creationTime)),
          formatDateTime(new Date(userFb.user.metadata.lastSignInTime)),
          false,
          true
        );
        await firebase
          .database()
          .ref('profiles')
          .push(profile);
        commit('setProfile', { profile });
        commit('createMessage', {
          text: 'Регистрация прошла успешно',
          type: 'success'
        });
        dispatch('verificationEmail');
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      } finally {
        commit('setLoading', { loading: false });
      }
    },

    async login({ commit, dispatch }, { email, password }) {
      commit('setLoading', { loading: true });
      try {
        const userFbAuth = await firebase
          .auth()
          .signInWithEmailAndPassword(email, password);
        const profileFbAuth = new Profile(
          userFbAuth.user.uid,
          userFbAuth.user.email,
          userFbAuth.user.emailVerified,
          userFbAuth.user.displayName,
          userFbAuth.user.photoURL,
          formatDateTime(new Date(userFbAuth.user.metadata.creationTime)),
          formatDateTime(new Date(userFbAuth.user.metadata.lastSignInTime))
        );
        commit('setProfile', { profile: profileFbAuth });
        dispatch('checkProfileFromFBDatabase', { profileFbAuth });
        commit('createMessage', { text: 'Вход выполнен', type: 'success' });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      } finally {
        commit('setLoading', { loading: false });
      }
    },

    synchronizeProfile({ dispatch }, { userFbAuth }) {
      const profileFbAuth = new Profile(
        userFbAuth.uid,
        userFbAuth.email,
        userFbAuth.emailVerified,
        userFbAuth.displayName,
        userFbAuth.photoURL,
        formatDateTime(new Date(userFbAuth.metadata.creationTime)),
        formatDateTime(new Date(userFbAuth.metadata.lastSignInTime))
      );
      dispatch('checkProfileFromFBDatabase', { profileFbAuth });
    },

    async checkProfileFromFBDatabase({ commit, dispatch }, { profileFbAuth }) {
      const profile = profileFbAuth;
      try {
        await firebase
          .database()
          .ref('/profiles/')
          .orderByChild('id')
          .equalTo(profile.id)
          .on('child_added', snapshot => {
            let profileFBDB = new Profile(
              snapshot.val().id,
              snapshot.val().email,
              snapshot.val().emailVerified,
              snapshot.val().displayName,
              snapshot.val().photoURL,
              formatDateTime(new Date(snapshot.val().creationTime)),
              formatDateTime(new Date(snapshot.val().lastSignInTime)),
              snapshot.val().isAdmin,
              snapshot.val().isActive
            );
            profile.isAdmin = profileFBDB.isAdmin;
            profile.isActive = profileFBDB.isActive;

            if (Profile.compare(profile, profileFBDB)) {
              dispatch('updateProfileInDb', {
                key: snapshot.key,
                profile: profile
              });
            } else {
              commit('setProfile', { profile });
            }
            if (profile.isAdmin) {
              dispatch('fetchProfileList');
            }
          });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      }
    },

    async updateProfileInDb({ commit }, { key, profile }) {
      await firebase
        .database()
        .ref('/profiles/')
        .child(key)
        .update({
          email: profile.email,
          emailVerified: profile.emailVerified,
          displayName: profile.displayName,
          photoURL: profile.photoURL,
          creationTime: profile.creationTime,
          lastSignInTime: profile.lastSignInTime,
          isAdmin: profile.isAdmin,
          isActive: profile.isActive
        })
        .then(() => {
          commit('setProfile', { profile });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        });
    },

    logout({ commit }) {
      commit('setLoading', { loading: true });
      firebase
        .auth()
        .signOut()
        .then(() => {
          commit('setProfile', { profile: null });
          commit('createMessage', { text: 'Выход выполнен', type: 'success' });
          commit('setLoading', { loading: false });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        });
    },

    resetPassword({ commit }) {
      commit('setLoading', { loading: true });
      firebase
        .auth()
        .sendPasswordResetEmail(firebase.auth().currentUser.email)
        .then(() => {
          commit('createMessage', {
            text: 'Отправлено сообщение на вашу почту для сброса пароля'
          });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        })
        .finally(() => {
          commit('setLoading', { loading: false });
        });
    },

    verificationEmail({ commit, dispatch }) {
      commit('setLoading', { loading: true });
      firebase
        .auth()
        .currentUser.sendEmailVerification()
        .then(() => {
          commit('createMessage', {
            text: 'Отправлено сообщение на ваш почтовый ящик для подтверждения'
          });
          dispatch('synchronizeProfile', {
            userFbAuth: firebase.auth().currentUser
          });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        })
        .finally(() => {
          commit('setLoading', { loading: false });
        });
    },

    updateEmail({ commit, dispatch }, email) {
      firebase
        .auth()
        .currentUser.updateEmail(email)
        .then(() => {
          commit('createMessage', {
            text: 'Адрес электронной почты для входа в приложение был изменен'
          });
          dispatch('synchronizeProfile', {
            userFbAuth: firebase.auth().currentUser
          });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        })
        .finally(() => {
          commit('setLoading', { loading: false });
        });
    },

    updateProfile({ commit, dispatch }, updates) {
      commit('setLoading', { loading: true });
      const currentUser = firebase.auth().currentUser;
      currentUser
        .updateProfile({
          displayName: updates.displayName,
          photoURL: updates.photoURL
        })
        .then(() => {
          commit('createMessage', {
            text: 'Ваш профиль был изменен',
            type: 'success'
          });
          dispatch('synchronizeProfile', {
            userFbAuth: firebase.auth().currentUser
          });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        });
      if (updates.email && updates.email !== currentUser.email) {
        dispatch('updateEmail', updates.email);
      } else {
        commit('setLoading', { loading: false });
      }
    },

    async uploadPhoto({ commit, dispatch }, photo) {
      commit('setLoading', { loading: true });

      try {
        if (photo) {
          const id = Date.now();
          const photoExt = photo.name.slice(photo.name.lastIndexOf('.'));
          const fileDate = await firebase
            .storage()
            .ref(`profiles/${id}${photoExt}`)
            .put(photo);
          const photoURL = await fileDate.ref.getDownloadURL().then(url => url);
          dispatch('updateProfile', { photoURL });
        } else {
          const photoURL = firebase.auth().currentUser.photoURL;
          await firebase
            .storage()
            .refFromURL(photoURL)
            .delete();
          dispatch('updateProfile', { photoURL: null });
        }
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      }
    },

    deleteProfile({ commit, dispatch }) {
      commit('setLoading', { loading: true });
      const currentUser = firebase.auth().currentUser;
      dispatch('deleteProfileInDb', { currentUser });
      currentUser
        .delete()
        .then(() => {
          dispatch('deleteProfileInDb', { currentUser });
        })
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        })
        .finally(() => {
          commit('setLoading', { loading: false });
        });
    },

    async deleteProfileInDb({ commit }, { currentUser }) {
      let key;
      await firebase
        .database()
        .ref('/profiles/')
        .orderByChild('id')
        .equalTo(currentUser.uid)
        .on('child_added', snapshot => {
          key = snapshot.key;
        });
      if (key) {
        await firebase
          .database()
          .ref('/profiles/')
          .child(key)
          .update({
            isActive: false
          })
          .then(() => {
            commit('setProfile', { profile: null });
          })
          .catch(error => {
            commit('createMessage', { text: error.message, type: 'error' });
            throw error;
          })
          .finally(() => {
            commit('createMessage', {
              text: 'Ваша учетная запись была удалена'
            });
            commit('setLoading', { loading: false });
          });
      }
    },
    async fetchProfileList({ commit }) {
      const profileList = [];
      try {
        const fbVal = await firebase
          .database()
          .ref('profiles')
          .once('value');
        const profiles = fbVal.val();
        if (profiles) {
          Object.keys(profiles).forEach(key => {
            const profile = profiles[key];
            profileList.push(
              new Profile(
                profile.id,
                profile.email,
                profile.emailVerified,
                profile.displayName,
                profile.photoURL,
                profile.creationTime,
                profile.lastSignInTime,
                profile.isAdmin,
                profile.isActive
              )
            );
          });
        }
        commit('setProfileList', { profiles: profileList });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      }
    }
  },
  getters: {
    profile(state) {
      return state.profile;
    },

    profileList(state) {
      return state.profileList;
    }
  }
};
