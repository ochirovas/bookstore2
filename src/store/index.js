import Vue from 'vue';
import Vuex from 'vuex';
import common from './common';
import product from './product';
import profile from './profile';
import review from './review';
import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    common,
    profile,
    product,
    review
  },
  plugins: [createPersistedState()],
  strict: false
});
