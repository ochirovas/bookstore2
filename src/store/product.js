import * as firebase from 'firebase';
import { ProductFactory } from '../data/product';
import { formatDateTime } from '../utils/date';

export default {
  state: {
    products: [],
    bucket: [],
    purchases: []
  },

  mutations: {
    setProducts(state, { products }) {
      state.products = products;
    },

    updateProduct(state, { product }) {
      const index = state.products.findIndex(p => {
        return p.id === product.id;
      });
      if (index !== -1) {
        state.products[index] = product;
      }
    },

    deleteProduct(state, id) {
      state.products = state.products.filter(product => product.id !== id);
    },

    synchronizeBucket(state) {
      state.bucket = state.bucket.map(productInBucket => {
        const product = state.products.find(p => p.id === productInBucket.id);
        if (product) return { ...product, count: productInBucket.count };
        else return productInBucket;
      });
    },

    addToBucket(state, { product }) {
      const productInBucket = state.bucket.find(p => p.id === product.id);
      if (productInBucket) {
        productInBucket.count++;
      } else {
        state.bucket.push({ ...product, count: 1 });
      }
    },

    removeFromBucket(state, { id, count = 1 }) {
      const index = state.bucket.findIndex(p => p.id === id);
      if (index !== -1) {
        state.bucket[index].count -= count;
        if (state.bucket[index].count < 1) {
          state.bucket.splice(index, 1);
        }
      }
    },

    setPurchases(state, { purchases }) {
      state.purchases = [...purchases];
    },

    addPurchases(state, { purchases }) {
      state.purchases = [...state.purchases, ...purchases];
    }
  },

  actions: {
    createProduct({ commit, dispatch }, { product, image, imageConfirm }) {
      try {
        const productFactory = new ProductFactory();
        const newProduct = productFactory.create(product);
        const key = firebase
          .database()
          .ref('products')
          .push(newProduct).key;
        commit('createMessage', { text: 'Товар создан', type: 'success' });
        if (imageConfirm) {
          newProduct.id = key;
          dispatch('uploadImage', { image: image, product: newProduct });
        }
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      }
    },

    async updateProduct(
      { commit, dispatch },
      { product, image, imageConfirm }
    ) {
      const productFactory = new ProductFactory();
      const productWithUpdates = productFactory.create(product);
      try {
        if (imageConfirm) {
          dispatch('uploadImage', {
            image: image,
            product: productWithUpdates
          });
        }
        await firebase
          .database()
          .ref('products')
          .child(productWithUpdates.id)
          .update(productWithUpdates);
        commit('updateProduct', { product: productWithUpdates });
        commit('createMessage', { text: 'Товар обновлен', type: 'success' });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
      }
    },

    async uploadImage({ commit, dispatch }, { image, product }) {
      try {
        if (image) {
          const id = Date.now();
          const photoExt = image.name.slice(image.name.lastIndexOf('.'));
          const file = await firebase
            .storage()
            .ref(`products/${product.id}/preview/${id}${photoExt}`)
            .put(image);
          product.imagePreview = await file.ref
            .getDownloadURL()
            .then(url => url);
          dispatch('updateProduct', { product });
        } else {
          await firebase
            .storage()
            .refFromURL(product.imagePreview)
            .delete()
            .catch(() => {});
          product.imagePreview = null;
          dispatch('updateProduct', { product });
        }
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      }
    },

    async deleteProduct({ commit }, id) {
      try {
        const productRef = await firebase
          .database()
          .ref('products')
          .child(id);
        const product = await productRef
          .once('value')
          .then(value => value.val());
        const imagePreview = product.imagePreview;
        productRef.remove();

        if (imagePreview) {
          const imageRef = await firebase.storage().refFromURL(imagePreview);
          imageRef.delete().catch(() => {});
        }

        commit('deleteProduct', id);
        commit('createMessage', { text: 'Товар удален', type: 'success' });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
      }
    },

    async fetchProducts({ commit }) {
      commit('setLoading', { loading: true });
      const products = [];
      commit('setProducts', { products: [] });
      try {
        const fbVal = await firebase
          .database()
          .ref('products')
          .once('value');
        const productsFromFb = fbVal.val();
        if (productsFromFb) {
          const productFactory = new ProductFactory();
          Object.keys(productsFromFb).forEach(key => {
            let product = productsFromFb[key];
            product.id = key;
            products.push(productFactory.create(product));
          });
        }
        commit('setProducts', { products });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      } finally {
        commit('setLoading', { loading: false });
        commit('synchronizeBucket');
      }
    },

    addToBucket({ commit }, payload) {
      commit('addToBucket', payload);
    },

    removeFromBucket({ commit }, payload) {
      commit('removeFromBucket', payload);
    },

    buy({ commit, dispatch, getters }, { profileId }) {
      const purchaseDate = formatDateTime(new Date());
      const purchases = [];
      const productsInBucket = getters.bucket;

      try {
        productsInBucket.forEach(productInBucket => {
          const product = getters.productById(productInBucket.id);
          if (productInBucket.count > product.availability) {
            productInBucket.count = product.availability;
          }
          product.availability -= productInBucket.count;
          if (productInBucket.count > 0) {
            const purchase = {
              productId: productInBucket.id,
              profileId: profileId,
              date: purchaseDate,
              count: productInBucket.count,
              cost: productInBucket.count * productInBucket.cost,
              name: productInBucket.name
            };
            firebase
              .database()
              .ref('purchases')
              .push(purchase);
            purchases.push(purchase);
            dispatch('updateProduct', { product });
          }
        });
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      } finally {
        commit('addPurchases', { purchases });
        purchases.forEach(purchase => {
          commit('removeFromBucket', {
            id: purchase.productId,
            count: purchase.count
          });
        });
      }
    },

    async fetchPurchases({ commit }, { profileId }) {
      const purchases = [];
      try {
        await firebase
          .database()
          .ref('purchases')
          .orderByChild('profileId')
          .equalTo(profileId)
          .once('value', dataSnapshot =>
            dataSnapshot.forEach(childDataSnapshot => {
              purchases.push(childDataSnapshot.val());
            })
          );
      } catch (error) {
        commit('createMessage', { text: error.message, type: 'error' });
        throw error;
      } finally {
        commit('setPurchases', { purchases });
      }
    }
  },

  getters: {
    products(state) {
      return state.products;
    },

    productsByCategoryId(state) {
      return categoryId => {
        return state.products.filter(
          product =>
            product.categories &&
            product.categories.includes(categoryId) === true
        );
      };
    },

    productsFilteredByCategoryIds(state) {
      return categoryIds => {
        return state.products.filter(
          product =>
            product.categories &&
            categoryIds.includes(
              product.categories[product.categories.length - 1]
            )
        );
      };
    },

    productById(state) {
      return id => {
        return state.products.find(product => product.id === id);
      };
    },

    productByName(state) {
      return value => {
        let result = [];

        if (!value || value.length < 3) {
          return result;
        }

        result = state.products.filter(
          product =>
            product.name &&
            product.name.toLowerCase().indexOf(value.toLowerCase()) > -1
        );

        return result;
      };
    },
    bucket(state) {
      return state.bucket;
    },

    productCountInBucket(getters) {
      return getters.bucket.reduce((previousValue, currentValue) => {
        previousValue += currentValue.count;
        return previousValue;
      }, 0);
    },

    isInBucket(state) {
      return id => {
        return !!state.bucket.find(item => item.id === id);
      };
    },

    purchases(state) {
      return state.purchases;
    }
  }
};
