export default {
  state: {
    drawer: {
      model: false,
      clipped: false,
      floating: false,
      mini: false
    },
    loading: false,
    message: null,
    theme: 'light'
  },

  mutations: {
    setLoading(state, { loading }) {
      state.loading = loading;
    },

    createMessage(state, { text = '', type = '' }) {
      if (text) state.message = { text, type };
      else state.message = null;
    },

    deleteMessage(state) {
      state.message = null;
    },

    switchTheme(state) {
      state.theme = state.theme === 'light' ? 'dark' : 'light';
    },

    switchDrawer(state) {
      state.drawer.model = !state.drawer.model;
    }
  },

  actions: {
    setLoading({ commit }, payload) {
      commit('setLoading', payload);
    },

    createMessage({ commit }, payload) {
      commit('createMessage', payload);
    },

    switchTheme({ commit }) {
      commit('switchTheme');
    },

    switchDrawer({ commit }) {
      commit('switchDrawer');
    }
  },

  getters: {
    loading(state) {
      return state.loading;
    },

    message(state) {
      return state.message;
    },

    theme(state) {
      return state.theme;
    },

    drawer(state) {
      return state.drawer;
    }
  }
};
