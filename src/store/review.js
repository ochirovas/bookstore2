import * as firebase from 'firebase';
import { formatDate } from '../utils/date';

export default {
  state: {
    reviews: []
  },

  mutations: {
    setReviews(state, { reviews }) {
      state.reviews = reviews || [];
    },

    addReview(state, { review }) {
      state.reviews.push(review);
    },
    deleteReview(state, { reviewId }) {
      const index = state.reviews.findIndex(review => review.id === reviewId);
      state.reviews.splice(index, 1);
    },
    updateReview(state, { reviewWithUpdates }) {
      const index = state.reviews.findIndex(
        review => review.id === reviewWithUpdates.id
      );
      state.reviews.splice(index, 1, reviewWithUpdates);
    }
  },

  actions: {
    async fetchReviewsByProduct({ commit }, { productId }) {
      const reviews = [];
      await firebase
        .database()
        .ref('reviews')
        .orderByChild('productId')
        .equalTo(productId)
        .once('value', dataSnapshot =>
          dataSnapshot.forEach(childDataSnapshot => {
            reviews.push(childDataSnapshot.val());
          })
        )
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        });
      commit('setReviews', { reviews });
    },

    async fetchReviewsByProfile({ commit }, { profileId }) {
      const reviews = [];
      await firebase
        .database()
        .ref('reviews')
        .orderByChild('profileId')
        .equalTo(profileId)
        .once('value', dataSnapshot =>
          dataSnapshot.forEach(childDataSnapshot => {
            reviews.push(childDataSnapshot.val());
          })
        )
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        });
      commit('setReviews', { reviews });
    },

    saveReview({ dispatch }, { review }) {
      if (review.id) {
        dispatch('updateReview', { reviewWithUpdates: review });
      } else {
        dispatch('createReview', { review });
      }
    },

    createReview({ commit, dispatch }, { review }) {
      review.id = firebase
        .database()
        .ref('reviews')
        .push(review).key;
      commit('addReview', { review });
      dispatch('updateReview', { reviewWithUpdates: review });
    },

    updateReview({ commit }, { reviewWithUpdates }) {
      reviewWithUpdates.date = formatDate(new Date());
      firebase
        .database()
        .ref('reviews')
        .child(reviewWithUpdates.id)
        .update(reviewWithUpdates)
        .catch(error => {
          commit('createMessage', { text: error.message, type: 'error' });
          throw error;
        });
      commit('updateReview', { reviewWithUpdates });
    },

    async deleteReview({ commit }, { reviewId }) {
      const reviewRef = await firebase
        .database()
        .ref('reviews')
        .child(reviewId);
      reviewRef.remove();
      commit('deleteReview', { reviewId });
    }
  },

  getters: {
    reviews(state) {
      return state.reviews;
    },
    profileReview(state) {
      return profileId => {
        return state.reviews.find(review => review.profileId === profileId);
      };
    }
  }
};
