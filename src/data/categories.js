export default {
  items: [
    { id: '00', icon: 'mdi-book-open-variant', name: 'Книги' },
    { id: '0000', parentId: '00', name: 'Художественная литература' },
    { id: '000000', parentId: '0000', name: 'Детективы, боевики' },
    {
      id: '00000001',
      parentId: '000000',
      name: 'Детективы',
      isChildless: true
    },
    { id: '00000002', parentId: '000000', name: 'Боевики', isChildless: true },
    { id: '000002', parentId: '0000', name: 'Приключения', isChildless: true },
    { id: '000003', parentId: '0000', name: 'Проза' },
    {
      id: '00000300',
      parentId: '000003',
      name: 'Классическая проза',
      isChildless: true
    },
    {
      id: '00000301',
      parentId: '000003',
      name: 'Современная проза',
      isChildless: true
    },
    {
      id: '000004',
      parentId: '0000',
      name: 'Фантастика, фэнтези, мистика, ужасы'
    },
    {
      id: '00000400',
      parentId: '000004',
      name: 'Мистика и ужасы',
      isChildless: true
    },
    {
      id: '00000401',
      parentId: '000004',
      name: 'Фантастика',
      isChildless: true
    },
    { id: '00000402', parentId: '000004', name: 'Фэнтези', isChildless: true },
    {
      id: '000005',
      parentId: '0000',
      name: 'Комиксы, графические романы. Манга'
    },
    {
      id: '00000500',
      parentId: '000005',
      name: 'Комиксы. Графические романы',
      isChildless: true
    },
    {
      id: '00000501',
      parentId: '000005',
      name: 'Манга',
      isChildless: true
    },
    { id: '000006', parentId: '0000', name: 'Афоризмы. Биографии и мемуары' },
    { id: '00000600', parentId: '000006', name: 'Афоризмы', isChildless: true },
    {
      id: '00000601',
      parentId: '000006',
      name: 'Биографии и мемуары',
      isChildless: true
    },
    {
      id: '000007',
      parentId: '0000',
      name: 'Поэзия',
      isChildless: true
    },
    { id: '0002', parentId: '00', name: 'Образование' },
    {
      id: '000200',
      parentId: '0002',
      name: 'Дошкольное образование',
      isChildless: true
    },
    {
      id: '000201',
      parentId: '0002',
      name: 'Начальная школа',
      isChildless: true
    },
    {
      id: '000202',
      parentId: '0002',
      name: 'Средняя школа',
      isChildless: true
    },
    { id: '000203', parentId: '0002', name: 'Высшая школа', isChildless: true },
    { id: '0003', parentId: '00', name: 'Наука и техника' },
    { id: '000300', parentId: '0003', name: 'Технические науки' },
    {
      id: '00030000',
      parentId: '000300',
      name: 'Общетехнические дисциплины',
      isChildless: true
    },
    {
      id: '00030001',
      parentId: '000300',
      name: 'Информатика. Компьютерная техника',
      isChildless: true
    },
    {
      id: '00030002',
      parentId: '000300',
      name: 'Автоматика. Радиоэлектроника. Связь',
      isChildless: true
    },
    {
      id: '00030003',
      parentId: '000300',
      name: 'Транспорт',
      isChildless: true
    },
    { id: '000301', parentId: '0003', name: 'Прикладные науки' },
    {
      id: '00030100',
      parentId: '000301',
      name: 'Строительство',
      isChildless: true
    },
    {
      id: '00030101',
      parentId: '000301',
      name: 'Энергетика. Промышленность',
      isChildless: true
    },
    {
      id: '00030102',
      parentId: '000301',
      name: 'Сельское хозяйство',
      isChildless: true
    },
    { id: '000302', parentId: '0003', name: 'Естественные науки' },
    {
      id: '00030200',
      parentId: '000302',
      name: 'Физико-математические науки',
      isChildless: true
    },
    { id: '00030201', parentId: '000302', name: 'Химия', isChildless: true },
    { id: '00030202', parentId: '000302', name: 'Экология', isChildless: true },
    { id: '00030203', parentId: '000302', name: 'Биология', isChildless: true },
    { id: '000303', parentId: '0003', name: 'Гуманитарные науки' },
    { id: '00030300', parentId: '000303', name: 'Теология', isChildless: true },
    {
      id: '00030301',
      parentId: '000303',
      name: 'Философия',
      isChildless: true
    },
    {
      id: '00030302',
      parentId: '000303',
      name: 'Психология',
      isChildless: true
    },
    {
      id: '00030303',
      parentId: '000303',
      name: 'Филология',
      isChildless: true
    },
    {
      id: '00030304',
      parentId: '000303',
      name: 'Искусствоведение',
      isChildless: true
    },
    { id: '000304', parentId: '0003', name: 'Медицина и фармакология' },
    { id: '00030400', parentId: '000304', name: 'Медицина', isChildless: true },
    {
      id: '00030401',
      parentId: '000304',
      name: 'Фармакология',
      isChildless: true
    },
    { id: '0004', parentId: '00', name: 'Общество' },
    { id: '000400', parentId: '0004', name: 'Культура', isChildless: true },
    { id: '000401', parentId: '0004', name: 'История' },
    {
      id: '00040100',
      parentId: '000401',
      name: 'Всемирная история',
      isChildless: true
    },
    {
      id: '00040101',
      parentId: '000401',
      name: 'Отечественная история',
      isChildless: true
    },
    { id: '000402', parentId: '0004', name: 'Военное дело', isChildless: true },
    {
      id: '000403',
      parentId: '0004',
      name: 'Право. Юридические науки',
      isChildless: true
    },
    {
      id: '000404',
      parentId: '0004',
      name: 'Социология',
      isChildless: true
    },
    {
      id: '000405',
      parentId: '0004',
      name: 'Политика',
      isChildless: true
    },

    { id: '0005', parentId: '00', name: 'Деловая литература' },
    {
      id: '000500',
      parentId: '0005',
      name: 'Экономика. Экономическая теория',
      isChildless: true
    },
    {
      id: '000501',
      parentId: '0005',
      name: 'Бухгалтерский и налоговый учет. Аудит'
    },
    {
      id: '00050100',
      parentId: '000501',
      name: 'Бухгалтерский учет',
      isChildless: true
    },
    { id: '00050103', parentId: '000501', name: 'Аудит', isChildless: true },
    {
      id: '000502',
      parentId: '0005',
      name: 'Управление финансами',
      isChildless: true
    },
    { id: '000503', parentId: '0005', name: 'Менеджмент', isChildless: true },
    { id: '000504', parentId: '0005', name: 'Маркетинг. Реклама' },
    {
      id: '00050400',
      parentId: '000504',
      name: 'Анализ рынка. Управление продажами, Клиентоориентированность',
      isChildless: true
    },
    {
      id: '00050401',
      parentId: '000504',
      name: 'Реклама. Копирайтинг',
      isChildless: true
    },
    {
      id: '00050403',
      parentId: '000504',
      name: 'PR. Брендинг',
      isChildless: true
    },
    { id: '000505', parentId: '0005', name: 'Бизнес. Торговля' },
    {
      id: '00050500',
      parentId: '000505',
      name: 'Торговля',
      isChildless: true
    },
    {
      id: '00050501',
      parentId: '000505',
      name: 'Биографии бизнесменов. Истории успеха кампаний и брендов',
      isChildless: true
    },
    {
      id: '00050502',
      parentId: '000505',
      name: 'Бизнес-планирование. Основы бизнеса',
      isChildless: true
    },
    { id: '0006', parentId: '00', name: 'Красота. Здоровье. Спорт' },
    { id: '000600', parentId: '0006', name: 'Здоровый образ жизни. Фитнес' },
    {
      id: '00060000',
      parentId: '000600',
      name: 'Фитнес. Аэробика. Йога. Пилатес',
      isChildless: true
    },
    {
      id: '00060001',
      parentId: '000600',
      name: 'Питание. Диеты',
      isChildless: true
    },
    {
      id: '00060002',
      parentId: '000600',
      name: 'Восточная медицина',
      isChildless: true
    },
    { id: '00060203', parentId: '000600', name: 'Массаж', isChildless: true },
    { id: '000601', parentId: '0006', name: 'Спорт. Самооборона' },
    {
      id: '00060100',
      parentId: '000601',
      name: 'Спорт',
      isChildless: true
    },
    {
      id: '00060101',
      parentId: '000601',
      name: 'Боевые искусства. Самооборона. Выживание',
      isChildless: true
    },
    { id: '000602', parentId: '0006', name: 'Внешность. Имидж. Этикет' },
    {
      id: '00060300',
      parentId: '000602',
      name: 'Уход за внешностью',
      isChildless: true
    },
    {
      id: '00060301',
      parentId: '000602',
      name: 'Гардероб. Мода. Стиль',
      isChildless: true
    },
    {
      id: '00060302',
      parentId: '000602',
      name: 'Имидж. Этикет',
      isChildless: true
    },
    { id: '0007', parentId: '00', name: 'Увлечения' },
    { id: '000700', parentId: '0007', name: 'Мой дом' },
    {
      id: '00070000',
      parentId: '000700',
      name: 'Быт. Полезные советы',
      isChildless: true
    },
    {
      id: '00070001',
      parentId: '000700',
      name: 'Интерьер. Декор',
      isChildless: true
    },
    {
      id: '00070002',
      parentId: '000700',
      name: 'Строительство. Ремонт',
      isChildless: true
    },
    {
      id: '00070003',
      parentId: '000700',
      name: 'Сад. Огород',
      isChildless: true
    },
    {
      id: '00070004',
      parentId: '000700',
      name: 'Домашний скот. Пчеловодство',
      isChildless: true
    },
    { id: '000701', parentId: '0007', name: 'Рукоделие. Ремесла' },
    {
      id: '00070100',
      parentId: '000701',
      name: 'Рукоделие',
      isChildless: true
    },
    { id: '00070101', parentId: '000701', name: 'Ремесла', isChildless: true },
    { id: '000702', parentId: '0007', name: 'Кулинария' },
    {
      id: '00070200',
      parentId: '000702',
      name: 'Кулинарные рецепты',
      isChildless: true
    },
    {
      id: '00070201',
      parentId: '000702',
      name: 'Выпечка. Десерты',
      isChildless: true
    },
    { id: '00070202', parentId: '000702', name: 'Напитки', isChildless: true },
    {
      id: '00070203',
      parentId: '000702',
      name: 'Консервирование',
      isChildless: true
    },
    {
      id: '00070204',
      parentId: '000702',
      name: 'Сервировка стола. Украшение блюд',
      isChildless: true
    },
    { id: '000703', parentId: '0007', name: 'Досуг. Хобби' },
    {
      id: '00070300',
      parentId: '000703',
      name: 'Популярные справочники для досуга',
      isChildless: true
    },
    {
      id: '00070301',
      parentId: '000703',
      name: 'Фотография',
      isChildless: true
    },
    {
      id: '00070302',
      parentId: '000703',
      name: 'Игры. Развлечения',
      isChildless: true
    },
    {
      id: '00070303',
      parentId: '000703',
      name: 'Коллекционирование',
      isChildless: true
    },
    {
      id: '00070304',
      parentId: '000703',
      name: 'Домашние животные',
      isChildless: true
    },
    {
      id: '00070305',
      parentId: '000703',
      name: 'Охота. Рыбалка. Сбор грибов и ягод',
      isChildless: true
    },
    { id: '000704', parentId: '0007', name: 'Туризм' },
    {
      id: '00070400',
      parentId: '000704',
      name: 'Введение в туризм',
      isChildless: true
    },
    {
      id: '00070401',
      parentId: '000704',
      name: 'Путеводители',
      isChildless: true
    },
    {
      id: '00070402',
      parentId: '000704',
      name: 'Карты городов, регионов и стран',
      isChildless: true
    },
    {
      id: '00070403',
      parentId: '000704',
      name: 'Атласы и карты автодорог',
      isChildless: true
    },
    {
      id: '00070404',
      parentId: '000704',
      name: 'Путевые очерки. Травелоги',
      isChildless: true
    },
    {
      id: '00070405',
      parentId: '000704',
      name: 'Подарочные издания о туризме',
      isChildless: true
    },
    { id: '000705', parentId: '0007', name: 'Личный транспорт' },
    {
      id: '00070500',
      parentId: '000705',
      name: 'Автомобиль',
      isChildless: true
    },
    {
      id: '00070501',
      parentId: '000705',
      name: 'ПДД. Права и обязанности водителя. Штрафы',
      isChildless: true
    },
    {
      id: '00070502',
      parentId: '000705',
      name: 'Подготовка к экзамену в ГИБДД',
      isChildless: true
    },
    { id: '01', icon: 'mdi-content-cut', name: 'Канцовары' },
    { id: '0100', parentId: '01', name: 'Галантерея' },
    {
      id: '010000',
      parentId: '0100',
      name: 'Визитницы, запасные блоки',
      isChildless: true
    },
    {
      id: '010001',
      parentId: '0100',
      name: 'Обложки для документов',
      isChildless: true
    },
    {
      id: '010002',
      parentId: '0100',
      name: 'Папки кожаные и кожзам',
      isChildless: true
    },
    {
      id: '010003',
      parentId: '0100',
      name: 'Портфели, папки-портфели деловые',
      isChildless: true
    },
    { id: '0101', parentId: '01', name: 'Офисные принадлежности' },
    { id: '010100', parentId: '0101', name: 'Клей', isChildless: true },
    {
      id: '010101',
      parentId: '0101',
      name: 'Металлоканцелярия',
      isChildless: true
    },
    {
      id: '010102',
      parentId: '0101',
      name: 'Офисная канцелярия',
      isChildless: true
    },
    {
      id: '010103',
      parentId: '0101',
      name: 'Штемпельная продукция',
      isChildless: true
    },
    {
      id: '010104',
      parentId: '0101',
      name: 'Лента клейкая',
      isChildless: true
    },
    {
      id: '010105',
      parentId: '0101',
      name: 'Хранение и архивация документов',
      isChildless: true
    },
    { id: '0102', parentId: '01', name: 'Чертежные принадлежности' },
    {
      id: '010200',
      parentId: '0102',
      name: 'Готовальни, циркули',
      isChildless: true
    },
    {
      id: '010201',
      parentId: '0102',
      name: 'Ластики, наборы',
      isChildless: true
    },
    { id: '010202', parentId: '0102', name: 'Линейки', isChildless: true },
    {
      id: '010203',
      parentId: '0102',
      name: 'Точилки, наборы',
      isChildless: true
    },
    { id: '0103', parentId: '01', name: 'Школьные принадлежности' },
    {
      id: '010300',
      parentId: '0103',
      name: 'Дидактический материал',
      isChildless: true
    },
    {
      id: '010301',
      parentId: '0103',
      name: 'Папки и обложки',
      isChildless: true
    },
    {
      id: '010302',
      parentId: '0103',
      name: 'Краски, кисти',
      isChildless: true
    },
    {
      id: '010303',
      parentId: '0103',
      name: 'Товары для лепки',
      isChildless: true
    },
    {
      id: '010304',
      parentId: '0103',
      name: 'Школьная галантерея',
      isChildless: true
    },
    { id: '0104', parentId: '01', name: 'Письменные принадлежности' },
    {
      id: '010400',
      parentId: '0104',
      name: 'Карандаши цветные',
      isChildless: true
    },
    {
      id: '010401',
      parentId: '0104',
      name: 'Карандаши чернографитные, грифели',
      isChildless: true
    },
    {
      id: '010402',
      parentId: '0104',
      name: 'Корректирующие средства и разбавители',
      isChildless: true
    },
    { id: '010403', parentId: '0104', name: 'Маркеры', isChildless: true },
    { id: '010404', parentId: '0104', name: 'Ручки', isChildless: true },
    { id: '010405', parentId: '0104', name: 'Стержни', isChildless: true },
    { id: '010406', parentId: '0104', name: 'Фломастеры', isChildless: true },
    { id: '0105', parentId: '01', name: 'Товары для художников' },
    { id: '010500', parentId: '0105', name: 'Бумага', isChildless: true },
    {
      id: '010501',
      parentId: '0105',
      name: 'Графические материалы',
      isChildless: true
    },
    { id: '010502', parentId: '0105', name: 'Живопись', isChildless: true },
    { id: '010503', parentId: '0105', name: 'Инструменты', isChildless: true },
    { id: '010504', parentId: '0105', name: 'Кисти', isChildless: true },
    { id: '010505', parentId: '0105', name: 'Лепка, гипс', isChildless: true },
    {
      id: '010506',
      parentId: '0105',
      name: 'Мольберты, планшеты, рамы',
      isChildless: true
    },
    {
      id: '010507',
      parentId: '0105',
      name: 'Папки и тубусы',
      isChildless: true
    },
    { id: '010508', parentId: '0105', name: 'Холсты', isChildless: true },
    {
      id: '010509',
      parentId: '0105',
      name: 'Чертежка и макетирование',
      isChildless: true
    },
    { id: '0106', parentId: '01', name: 'Бумажные изделия' },
    {
      id: '010600',
      parentId: '0106',
      name: 'Бланки, конверты, учетные книги',
      isChildless: true
    },
    {
      id: '010601',
      parentId: '0106',
      name: 'Бумага и картон',
      isChildless: true
    },
    {
      id: '010602',
      parentId: '0106',
      name: 'Бумага офисная',
      isChildless: true
    },
    {
      id: '010603',
      parentId: '0106',
      name: 'Для рисования и черчения',
      isChildless: true
    },
    { id: '010604', parentId: '0106', name: 'Дневники', isChildless: true },
    {
      id: '010605',
      parentId: '0106',
      name: 'Ежедневники, планинги, органайзеры',
      isChildless: true
    },
    { id: '010606', parentId: '0106', name: 'Тетради', isChildless: true },
    {
      id: '010607',
      parentId: '0106',
      name: 'Книги для записей, блокноты',
      isChildless: true
    },
    { id: '02', icon: 'mdi-gift', name: 'Сувениры' },
    { id: '0200', parentId: '02', name: 'Открытки', isChildless: true },
    { id: '0201', parentId: '02', name: 'Календари', isChildless: true },
    {
      id: '0202',
      parentId: '02',
      name: 'Упаковочные изделия',
      isChildless: true
    },
    { id: '03', icon: 'mdi-gamepad-variant', name: 'Игры' },
    { id: '0300', parentId: '03', name: 'Конструкторы', isChildless: true },
    { id: '0301', parentId: '03', name: 'Настольные игры', isChildless: true },
    { id: '0302', parentId: '03', name: 'Пазлы', isChildless: true },
    { id: '0303', parentId: '03', name: 'Головоломки', isChildless: true },
    { id: '0304', parentId: '03', name: 'Сборные модели', isChildless: true },
    { id: '0305', parentId: '03', name: 'Мягкие игрушки', isChildless: true }
  ]
};
