export const productAttributes = [
  {
    key: 'id',
    type: 'text-field',
    visibility: 'hidden',
    value: 'ID товара',
    order: 0
  },
  {
    key: 'name',
    type: 'text-field',
    visibility: 'preview',
    value: 'Наименование',
    order: 1
  },
  {
    key: 'author',
    type: 'text-field',
    visibility: 'preview',
    value: 'Автор',
    order: 2
  },
  {
    key: 'series',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Цикл/Серия',
    order: 3
  },
  {
    key: 'isbn',
    type: 'text-field',
    visibility: 'list-item',
    value: 'ISBN',
    order: 4
  },
  {
    key: 'producer',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Издательство/Производство',
    order: 5
  },
  {
    key: 'year',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Год издания',
    order: 6
  },
  {
    key: 'translator',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Переводчик',
    order: 7
  },
  {
    key: 'pages',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Количество страниц',
    order: 8
  },
  {
    key: 'format',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Формат',
    order: 9
  },
  {
    key: 'annotation',
    type: 'text-area',
    visibility: 'additional',
    value: 'Аннотация',
    order: 10
  },
  {
    key: 'cover',
    type: 'select',
    visibility: 'list-item',
    value: 'Тип обложки',
    order: 11
  },
  {
    key: 'edition',
    type: 'text-field',
    visibility: 'additional',
    value: 'Тираж',
    order: 12
  },
  {
    key: 'weight',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Вес, г',
    order: 13
  },
  {
    key: 'age',
    type: 'text-field',
    visibility: 'list-item',
    value: 'Возрастные ограничения',
    order: 14
  },
  {
    key: 'imagePreview',
    type: 'image',
    visibility: 'additional',
    value: 'Обложка',
    order: 15
  },
  {
    key: 'categories',
    type: 'select',
    visibility: 'additional',
    value: 'Категории',
    order: 16
  },
  {
    key: 'releaseDate',
    type: 'text-field',
    visibility: 'additional',
    value: 'Дата поступления товара',
    order: 17
  },
  {
    key: 'availability',
    type: 'text-field',
    visibility: 'preview',
    value: 'В наличии',
    order: 18
  },
  {
    key: 'cost',
    type: 'text-field',
    visibility: 'preview',
    value: 'Цена, ₽',
    order: 19
  }
];

export const listTypes = [
  { key: 'new', value: 'Новинки' },
  { key: 'popular', value: 'Популярное за месяц' },
  { key: 'soon', value: 'Скоро в продаже' },
  { key: 'discounts', value: 'Со скидками' }
];

export const ageLimits = ['0+', '6+', '12+', '16+', '18+'];

export const coverTypes = [
  'Мягкая бумажная',
  'Твердая бумажная',
  'Мягкая глянцевая',
  'Твердая глянцевая',
  'Суперобложка'
];
