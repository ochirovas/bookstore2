export default class Profile {
  constructor(
    id = null,
    email = null,
    emailVerified = false,
    displayName = '',
    photoURL = null,
    creationTime = null,
    lastSignInTime = null,
    isAdmin = false,
    isActive = true
  ) {
    this.id = id;
    this.email = email;
    this.emailVerified = emailVerified;
    this.displayName = displayName;
    this.photoURL = photoURL;
    this.creationTime = creationTime;
    this.lastSignInTime = lastSignInTime;
    this.isAdmin = isAdmin;
    this.isActive = isActive;
  }
  static compare(profileA, profileB) {
    return JSON.stringify(profileA) !== JSON.stringify(profileB);
  }
}
