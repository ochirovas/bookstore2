export class Product {
  constructor(
    id = null,
    cost = null,
    name = null,
    author = null,
    producer = null,
    series = null,
    year = null,
    isbn = null,
    translator = null,
    pages = null,
    edition = null,
    format = null,
    weight = null,
    cover = null,
    age = null,
    annotation = null,
    imagePreview = null,
    categories = null,
    releaseDate = null,
    availability = 0
  ) {
    this.id = id;
    this.cost = cost;
    this.name = name;
    this.author = author;
    this.producer = producer;
    this.series = series;
    this.year = year;
    this.isbn = isbn;
    this.translator = translator;
    this.pages = pages;
    this.edition = edition;
    this.format = format;
    this.weight = weight;
    this.cover = cover;
    this.age = age;
    this.annotation = annotation;
    this.imagePreview = imagePreview;
    this.categories = categories;
    this.releaseDate = releaseDate;
    this.availability = availability;
  }
}

export class ProductFactory {
  create(product) {
    return new Product(
      product.id,
      product.cost,
      product.name,
      product.author,
      product.producer,
      product.series,
      product.year,
      product.isbn,
      product.translator,
      product.pages,
      product.edition,
      product.format,
      product.weight,
      product.cover,
      product.age,
      product.annotation,
      product.imagePreview,
      product.categories,
      product.releaseDate,
      product.availability
    );
  }
}
