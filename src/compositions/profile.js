import { computed } from '@vue/composition-api';
import store from '../store';
import router from '../router';

export function useProfile() {
  const profile = computed(() => store.getters.profile);
  const profileList = computed(() => store.getters.profileList);

  const registerProfile = data => {
    store.dispatch('registerNew', data).then(() => {
      router.push('/').catch(() => {});
    });
  };
  const login = data => {
    store.dispatch('login', data).then(() => {
      router.push('/').catch(() => {});
    });
  };

  const updateProfile = updates => {
    store.dispatch('updateProfile', updates).then(() => {
      router.push('/').catch(() => {});
    });
  };

  const uploadPhoto = photo => {
    store.dispatch('uploadPhoto', photo);
  };

  const deleteProfile = () => {
    store.dispatch('deleteProfile').then(() => {
      store.dispatch('logout').then(() => {
        router.push('/').catch(() => {});
      });
    });
  };

  const synchronizeProfile = userFbAuth =>
    store.dispatch('synchronizeProfile', { userFbAuth });

  const logout = () => {
    store.dispatch('logout').then(() => {
      router.push('/').catch(() => {});
    });
  };

  const verificationEmail = () => {
    store.dispatch('verificationEmail');
  };

  const resetPassword = () => {
    store.dispatch('resetPassword');
  };

  return {
    profile,
    profileList,
    registerProfile,
    login,
    updateProfile,
    uploadPhoto,
    deleteProfile,
    synchronizeProfile,
    logout,
    verificationEmail,
    resetPassword
  };
}
