import { computed } from '@vue/composition-api';
import store from '../store';
import vuetify from '../plugin/vuetify';

export function useLoading() {
  const loading = computed(() => store.getters.loading);

  return {
    loading
  };
}

export function useSnackbar() {
  const deleteMessage = () => store.dispatch('createMessage', {});
  const message = computed(() => store.getters.message);

  return { deleteMessage, message };
}

export function useTheme() {
  const theme = computed(() => {
    const theme = store.getters.theme;
    vuetify.framework.theme.dark = theme === 'dark';
    return theme;
  });

  const switchTheme = () => {
    vuetify.framework.theme.dark = !vuetify.framework.theme.dark;
    store.dispatch('switchTheme');
  };

  const themeColors = computed(
    () => vuetify.framework.theme.themes[theme.value]
  );

  return {
    theme,
    switchTheme,
    themeColors
  };
}

export function useDrawer() {
  const drawer = computed(() => store.getters.drawer);
  const switchDrawer = () => store.dispatch('switchDrawer');
  return {
    drawer,
    switchDrawer
  };
}

export function useBreakpoint() {
  const breakpoint = () => {
    return {
      name: vuetify.framework.breakpoint.name,
      width: vuetify.framework.breakpoint.width,
      height: vuetify.framework.breakpoint.height
    };
  };

  return {
    breakpoint
  };
}

export function useValidator() {
  const nullValidation = ({ str }) => !!str || 'Необходимо заполнить поле';

  const minLengthValidation = ({ str, min }) => {
    return (
      (!!str && !!min && str.length >= min) ||
      `Количество символов должно не должно быть меньше ${min}`
    );
  };

  const maxLengthValidation = ({ str, max }) => {
    return (
      (!!str && !!max && str.length <= max) ||
      `Количество символов должно не должно быть больше ${max}`
    );
  };

  const emailValidation = ({ str }) => {
    return (
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        str
      ) || 'Необходимо указать корректный адрес'
    );
  };

  const positiveNumberValidation = ({ num }) => {
    return (!!num && num > 0) || 'Необходимо положительное число';
  };

  return {
    nullValidation,
    minLengthValidation,
    maxLengthValidation,
    emailValidation,
    positiveNumberValidation
  };
}

import {
  productAttributes,
  ageLimits,
  coverTypes
} from '../data/attributes.js';

export function useAttributes() {
  const attributes = () =>
    [...productAttributes].sort((attrA, attrB) =>
      attrA.order <= attrB.order ? -1 : 1
    );

  const attributesByKeys = keys => {
    return attributes().filter(
      atr =>
        keys.includes(atr.key) &&
        (atr.visibility === 'list-item' || atr.visibility === 'preview')
    );
  };

  return {
    productAttributes: attributes,
    productAttributesByKeys: attributesByKeys,
    ageLimits: [...ageLimits],
    coverTypes: [...coverTypes]
  };
}
