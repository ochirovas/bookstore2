import store from '../store';
import { computed } from '@vue/composition-api';

export function useReviews() {
  const reviewsByProduct = productId =>
    store.dispatch('fetchReviewsByProduct', { productId });

  const reviewsByProfile = profileId =>
    store.dispatch('fetchReviewsByProfile', { profileId });

  const saveReview = review => {
    store.dispatch('saveReview', { review });
  };

  const deleteReview = reviewId => {
    store.dispatch('deleteReview', { reviewId });
  };

  const reviews = computed(() => store.getters.reviews);

  const profileReview = profileId => store.getters.profileReview(profileId);

  return {
    reviewsByProduct,
    reviewsByProfile,
    saveReview,
    deleteReview,
    reviews,
    profileReview
  };
}
