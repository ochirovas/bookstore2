import store from '../store';
import { computed } from '@vue/composition-api';
import router from '../router';

export function useProducts() {
  const products = computed(() => store.getters.products);
  const productById = id => store.getters.productById(id);
  const productsByCategoryId = id => store.getters.productsByCategoryId(id);
  const productsFilteredByCategoryIds = categoryIds =>
    store.getters.productsFilteredByCategoryIds(categoryIds);
  const productByName = title => store.getters.productByName(title);

  const fetchProducts = () => store.dispatch('fetchProducts');

  const createProduct = ({ product, image, imageConfirm }) =>
    store.dispatch('createProduct', {
      product,
      image,
      imageConfirm
    });

  const updateProduct = ({ product, image, imageConfirm }) =>
    store.dispatch('updateProduct', {
      product,
      image,
      imageConfirm
    });

  const deleteProduct = id => store.dispatch('deleteProduct', id);

  return {
    products,
    productById,
    productsByCategoryId,
    productsFilteredByCategoryIds,
    productByName,
    fetchProducts,
    createProduct,
    updateProduct,
    deleteProduct
  };
}

export function useBucket() {
  const bucket = computed(() => store.getters.bucket);
  const productCountInBucket = computed(
    () => store.getters.productCountInBucket
  );

  const isInBucket = computed(() => {
    return id => {
      return store.getters.isInBucket(id);
    };
  });
  const addToBucket = product => {
    return store.dispatch('addToBucket', { product });
  };
  const removeFromBucket = ({ id, count }) => {
    store.dispatch('removeFromBucket', { id, count });
  };

  const buy = () => {
    const profile = store.getters.profile;
    if (profile && profile.id) {
      store.dispatch('buy', {
        profileId: profile.id
      });
    } else {
      router.push('/login').catch(() => {});
    }
  };

  return {
    bucket,
    productCountInBucket,
    isInBucket,
    addToBucket,
    removeFromBucket,
    buy
  };
}

export function usePurchases() {
  const purchases = computed(() => store.getters.purchases);

  const fetchPurchases = profileId =>
    store.dispatch('fetchPurchases', { profileId });

  return {
    fetchPurchases,
    purchases
  };
}
