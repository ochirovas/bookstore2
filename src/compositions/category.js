import categoriesList from '../data/categories';

export function useCategories() {
  const categories = () => {
    return [...categoriesList.items];
  };

  const categoryById = id => {
    return categories().find(category => category.id === id);
  };

  const subCategoriesById = id => {
    return categories()
      .filter(category => category.parentId === id)
      .sort(compare);
  };

  const mainCategories = () => {
    return categories().filter(category => !category.parentId);
  };

  const finalSubCategories = () => {
    return categories()
      .filter(category => category.isChildless === true)
      .map(category => {
        category = { ...category };
        let category_ = category;
        let description = '';
        let path = [category.id];
        while (category_.parentId) {
          category_ = categories().find(c => c.id === category_.parentId);
          path.unshift(category_.id);
          description = ` /${category_.name} ${description}`;
        }
        category.description = description;
        category.path = path;
        category.icon = category_.icon;
        return category;
      })
      .sort(compare);
  };

  const categoryTree = () => {
    const categoryTree = mainCategories();
    const findChildren = ob => {
      for (let child of ob) {
        child.children = subCategoriesById(child.id);
        findChildren(child.children);
      }
    };
    findChildren(categoryTree);
    return categoryTree;
  };

  return {
    categories,
    categoryById,
    subCategoriesById,
    mainCategories,
    finalSubCategories,
    categoryTree
  };
}

const compare = (categoryA, categoryB) => {
  if (categoryA.id < categoryB.id) {
    return -1;
  } else if (categoryA.id > categoryB.id) {
    return 1;
  } else {
    return 0;
  }
};
