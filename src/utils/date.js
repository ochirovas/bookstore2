export const formatDate = date => {
  const year = leadingZeroCheck(date.getFullYear());
  const month = leadingZeroCheck(date.getMonth() + 1);
  const day = leadingZeroCheck(date.getDate());

  return `${year}.${month}.${day}`;
};

export const formatDateTime = date => {
  const year = leadingZeroCheck(date.getFullYear());
  const month = leadingZeroCheck(date.getMonth() + 1);
  const day = leadingZeroCheck(date.getDate());
  const hours = leadingZeroCheck(date.getHours());
  const minutes = leadingZeroCheck(date.getMinutes());
  const seconds = leadingZeroCheck(date.getSeconds());

  return `${year}.${month}.${day} ${hours}:${minutes}:${seconds}`;
};

const leadingZeroCheck = value => (value < 10 ? '0' + value : value);
