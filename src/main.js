import Vue from 'vue';
import VueCompositionApi, { onBeforeMount } from '@vue/composition-api';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugin/vuetify';
import firebaseConfig from './firebaseConfig';
import { useSnackbar } from './compositions/common';
import { useProducts, usePurchases } from './compositions/product';
import { useProfile } from './compositions/profile';

Vue.config.productionTip = false;
Vue.use(VueCompositionApi);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),
  setup() {
    async function initFirebase() {
      const { default: firebase } = await import('firebase');
      firebase.initializeApp(firebaseConfig);
      firebase.analytics();
      firebase.auth().languageCode = 'ru';

      return firebase;
    }

    onBeforeMount(() => {
      useSnackbar().deleteMessage();
      initFirebase().then(firebase => {
        useProducts().fetchProducts();
        firebase.auth().onAuthStateChanged(userFbAuth => {
          if (userFbAuth) {
            useProfile()
              .synchronizeProfile(userFbAuth)
              .then(() => {
                usePurchases().fetchPurchases(useProfile().profile.value.id);
              });
          }
        });
      });
    });
  }
}).$mount('#app');
