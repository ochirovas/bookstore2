import { ref } from '@vue/composition-api';
import { useProfile } from '../compositions/profile';

export default function(to, from, next) {
  const profile = ref(useProfile().profile);
  if (profile.value && profile.value.isAdmin) {
    next();
  } else {
    next('/login');
  }
}
