import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

import AuthGuard from './auth-guard';
import AdminGuard from './admin-guard';

Vue.use(VueRouter);

const routes = [
  {
    path: '',
    name: 'home',
    component: Home
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/auth/Login')
  },
  {
    path: '/registration',
    name: 'registration',
    component: () => import('../views/auth/Registration')
  },
  {
    path: '/profile',
    name: 'profile',
    component: () => import('../views/auth/Profile'),
    beforeEnter: AuthGuard
  },
  {
    path: '/profileList',
    name: 'profileList',
    component: () => import('../views/admin/ProfileList'),
    beforeEnter: AdminGuard
  },
  {
    path: '/productCategory/:id',
    props: true,
    name: 'productCategory',
    component: () => import('../views/ProductCategory')
  },
  {
    path: '/product/:id',
    props: true,
    name: 'product',
    component: () => import('../views/Product')
  },
  {
    path: '/productList',
    name: 'productList',
    component: () => import('../views/admin/ProductList'),
    beforeEnter: AdminGuard
  },
  {
    path: '/bucket',
    name: 'bucket',
    component: () => import('../views/Bucket')
  },
  {
    path: '/search',
    name: 'search',
    component: () => import('../views/ProductSearch')
  },
  {
    path: '*',
    name: '404',
    component: () => import('../views/PageNotFound')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    } else {
      return { x: 0, y: 0 };
    }
  }
});

export default router;
