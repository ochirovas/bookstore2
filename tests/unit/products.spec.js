import Vuex from 'vuex';
import { createLocalVue, shallowMount } from '@vue/test-utils';
import Product from '@/views/Product';
import vuetify from '@/plugin/vuetify';

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(vuetify);

const store = new Vuex.Store({
  state: {
    products: [
      {
        id: '-M4x1a_HbIx4EjofSvQc',
        cost: '556',
        name: 'Мудрость Черчилля. Цитаты великого политика',
        author: 'Уинстон Черчилль',
        producer: 'Эксмо',
        series: '',
        year: '2020',
        isbn: '978-5-04-110049-0',
        translator: null,
        pages: '480',
        edition: '3000',
        format: '20.5 x 14 x 2.9',
        weight: '640',
        cover: 'Твердая бумажная',
        age: '0+',
        annotation:
          'Единственный британский премьер-министр, получивший Нобелевскую премию по литературе',
        imagePreview:
          'https://firebasestorage.googleapis.com/v0/b/bookstorebase-87a22.appspot.com/o/products%2F-M4x1a_HbIx4EjofSvQc%2Fpreview%2F1593770346086.jpg?alt=media&token=04fb20bc-b25f-4a0b-8362-cc82b191705a',
        categories: ['00', '0000', '000006', '00000600'],
        releaseDate: '2020-04-15',
        availability: 10
      }
    ],
    bucket: [],
    purchases: [],
    profile: {
      id: 'Js6jmh2qYfTkknLyppirF23bW9R2',
      email: 'ochirovalexander@gmail.com',
      emailVerified: true,
      displayName: 'Александр Очиров',
      photoURL:
        'https://firebasestorage.googleapis.com/v0/b/bookstorebase-87a22.appspot.com/o/profiles%2F1597824335022.jpg?alt=media&token=7614bc7f-5dae-4cb7-a127-85d9a3bee53c',
      creationTime: '2020.07.13 11:06:03',
      lastSignInTime: '2020.09.02 00:09:05',
      isAdmin: true,
      isActive: true
    },
    drawer: {
      model: false,
      clipped: false,
      floating: false,
      mini: false
    },
    loading: false,
    message: null,
    theme: 'light'
  },

  mutations: {},

  actions: {}
});

beforeAll(() => {});

describe('Product.vue', () => {
  it('отрисовывает имя пользователя из настоящего Vuex хранилища', () => {
    const wrapper = shallowMount(Product, {
      store,
      localVue,
      vuetify,
      propsData: { id: '-M4x1a_HbIx4EjofSvQc' }
    });
    expect(wrapper.text()).toMatch(
      'Мудрость Черчилля. Цитаты великого политика'
    );
  });
});
